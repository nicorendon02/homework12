let exercise6 = () => {
    let numero = prompt("Ingresa un numero: ");
    if (parseInt(numero.charAt(numero.length-1))==0 || parseInt(numero.charAt(numero.length-1))==5)
    {
        console.log(`El numero ${numero} es divisible por 5`);
        return alert(`El numero ${numero} es divisible por 5`);
    }
    else{
        console.log(`El numero ${numero} NO es divisible por 5`);
        return alert(`El numero ${numero} NO es divisible por 5`);
    }
}

let exercise14 = () => {
    let numero = prompt("Ingresa un entero entre 1 y 10: ");
    let numeroRandom = Math.floor(Math.random() * 10) + 1;
    if (numeroRandom == numero)
    {
        console.log(`${numero} == ${numeroRandom} BUEN TRABAJO`);
        return alert(`${numero} == ${numeroRandom} BUEN TRABAJO`);
    }
    else{
        console.log(`${numero} != ${numeroRandom} NO CORRESPONDE`);
        return alert(`${numero} != ${numeroRandom} NO CORRESPONDE`);
    }
}

let exercise22 = () => {
    let numero = prompt("Ingresa un numero: ");
    
    var letras="abcdefghyjklmnñopqrstuvwxyz";

    function tiene_letras(numero){
    numero = numero.toLowerCase();
    for(i=0; i<numero.length; i++){
        if (letras.indexOf(numero.charAt(i),0)!=-1){
            return 1;
        }
    }
    return 0;
    }

    if(tiene_letras(numero) == 1)
    {
        console.log(`${numero} contiene letras`);
        alert(`${numero} contiene letras`);
        exercise22();
    }
    else
    {
        return alert(`${numero} tiene ${numero.length} digitos y NO contiene letras.`);
    }
}

let exercise29 = () => {
    // pedir al usuario cuantos numeros perfectos quiere ver
    const n = prompt("¿Cuantos numeros primos quieres? ");
    if(n <= 0)
    {
        return alert("El numero debe ser mayor a cero");
    }
    
    let numero = 2;
    let listNumPrim = [];

    function esPrimo(numero){
        for(let cont=0; cont<=n; cont++){    
            // Excepciones que no son primos
            if (numero == 4 || numero == 27) {
                numero++;
                continue;
            }
            for (let x = 2; x < numero; x++) {
                /* Si es divisible por cualquiera de estos números, no
                es primo */
                if (numero % x == 0){
                    numero++;
                    continue;
                }
            }
            // Si no se pudo dividir por ninguno de los de arriba, sí es primo
            listNumPrim.push(numero);
            numero++;
        }
    }
    esPrimo(numero);
    console.log(listNumPrim.length);
    return alert(`Tus numeros primos son:\n ${listNumPrim}`);
}

let exercise38 = () => {
    max=17;
    t="*";
    kg="&nbsp;";
    for(i=1;i<=max;i++){
        // Calcular cuántos espacios se necesitan al comienzo de cada línea
        // max/2==3.5；
        // Después de Math.ceil (3.5) == 4;
        // Math.abs (4-i) == [3,2,1,0,1,2,3]; este es el número de espacios por línea
        temp=Math.abs(Math.ceil(max/2)-i);
        for(j=1;j<=temp;j++){	// Imprimir espacios en un bucle;
            document.write(kg);
        }
        for(j=1;j<=max-temp*2;j++){	// Impresión de ciclo *
            document.write(t);
        }
        document.write("<br>") // Fin de una línea, nueva línea de salida
    }
}

let exercise46 = () => {
    const numeros = [1,2,3,4,5,6];
    numeros.forEach(numero => console.log(numero));
}

let exercise54 = () => {
    let str = prompt('Digita elementos separados por comas: ');
    let arr = str.split(','); 
    console.log(arr);
    return alert('['+arr.toString()+']');
}

let exercise60 = () => {
    return alert("Este ejercicio se ejecuta en un archivo externo");
}

let exercise61 = () => {
    return alert("Este ejercicio se ejecuta en un archivo externo");
}